@extends('layout.master')

@section('judul')
<h1>Halaman Form</h1>

@endsection

@section('content')  
    
    <form action="/halaman.welcome" method="post">
        @csrf
        <Label>First name:</Label><br><br>
           <input type="text" name="name"><br><br>
        <label>Last name:</label><br><br>
            <input type="text"name="last name"><br><br>
    
        <label>Gender:</label><br><br>
            <input type="radio" name="gender" value="Male">Male<br>
            <input type="radio" name="gender" value="Female">Female<br>
            <input type="radio" name="gender" value="Other">Other<br><br>
    

        <label>Nationality</label><br><br>
             <select name="Nationality">
                <option value="Ind">Indonesia</option>
                <option value="USA">Amerika</option>
                <option value="UK">Inggris</option>
            </select><br><br>

        <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="Language" value="1">Bahasa indonesia <br>
            <input type="checkbox" name="Language" value="2">English <br>
            <input type="checkbox" name="Language" value="3">Other <br><br>

        <Label> Bio:</Label> <br><br>           
            <textarea name="Bio" cols="30" rows="10"></textarea>><br><br>
        <Input type="submit" value="sign up">

    </form>

@endsection
