@extends('layout.master')

@section('judul')
Edit Cast {{$cast->nama}}

@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" class="form-control" value="{{$cast->nama}}" name="nama"  placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">umur</label>
        <input type="text" class="form-control" value="{{$cast->umur}}" name="umur"  placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
        
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection
        